const express = require("express");
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const fileDb = require("../fileDb");
const config = require("../config");


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({storage});

const router = express.Router();

router.get("/", (req, res) => {
  const messages = fileDb.getItems();
  res.send(messages);
});

router.post("/messages", upload.single("image"), (req, res) => {
  if (req.body.message === "") {
    return res.status(400).send({error: "Message can not be empty"});
  }

  const message = req.body;

  if (req.body.author === "") {
    message.author = "Anonymous";
  }

  if (req.file) {
    message.image = req.file.filename;
  } else {
    message.image = null;
  }

  fileDb.addItem(message);
  res.send(message);
});

module.exports = router;