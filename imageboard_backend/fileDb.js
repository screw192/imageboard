const fs = require("fs");
const {nanoid} = require("nanoid");


const filename = "./db.json";

let messagesData = [];

module.exports = {
  init() {
    try {
      const messages = fs.readFileSync(filename);
      messagesData = JSON.parse(messages);
    } catch (e) {
      messagesData = [];
    }
  },
  getItems() {
    return messagesData;
  },
  addItem(message) {
    message.id = nanoid();
    message.datetime = new Date().toISOString();

    messagesData.push(message);
    this.save();
  },
  save() {
    fs.writeFileSync(filename, JSON.stringify(messagesData, null, 2));
  }
}