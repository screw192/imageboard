import React from "react";
import {useDispatch} from "react-redux";
import CssBaseline from "@material-ui/core/CssBaseline";

import {fetchMessages, postMessage} from "./storage/actions/messagesActions";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import Messages from "./containers/Messages/Messages";
import NewMessageForm from "./components/NewMessageForm/NewMessageForm";


const App = () => {
  const dispatch = useDispatch();

  const onMessageSubmit = async messageData => {
    await dispatch(postMessage(messageData));
    dispatch(fetchMessages());
  };

  return (
      <>
        <CssBaseline/>
        <header>
          <AppToolbar/>
        </header>
        <main>
          <Messages/>
          <NewMessageForm
              onSubmit={onMessageSubmit}
          />
        </main>
      </>
  );
};

export default App;