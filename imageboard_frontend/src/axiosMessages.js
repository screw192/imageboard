import axios from "axios";
import {apiURL} from "./config";


const axiosMessages = axios.create({
  baseURL: apiURL
});

export default axiosMessages;