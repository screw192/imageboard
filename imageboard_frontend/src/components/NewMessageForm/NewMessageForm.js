import React, {useState} from 'react';
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";

import FileInput from "../UI/Form/FileInput";


const useStyles = makeStyles({
  container: {
    marginTop: "30px",
    marginBottom: "50px"
  },
  title: {
    color: "#ff6600",
    marginBottom: "15px",
    cursor: "pointer",
  },
  paper: {
    padding: "10px",
  },
});

const initialState = {
  author: "",
  message: "",
  image: ""
};

const NewMessageForm = ({onSubmit}) => {
  const classes = useStyles();

  const [message, setMessage] = useState(initialState);
  const [formVisible, setFormVisible] = useState(false);

  const submitFormHandler = e => {
    e.preventDefault();

    const newMessageData = new FormData();

    Object.keys(message).forEach(key => {
      newMessageData.append(key, message[key]);
    });

    onSubmit(newMessageData);
    setMessage({...initialState});
  };

  const onInputChange = e => {
    const key = e.target.name;
    const value = e.target.value;

    setMessage( prevState => ({
      ...prevState,
      [key]: value
    }));
  };

  const fileChangeHandler = e => {
    const key = e.target.name;
    const file = e.target.files[0];

    setMessage( prevState => ({
      ...prevState,
      [key]: file
    }));
  };

  const toggleForm = () => {
    setFormVisible(!formVisible);
  };

  let title;
  formVisible ? title = "Закрыть форму постинга" : title = "Ответить в тред";
  let formDisplay;
  formVisible ? formDisplay = "block" : formDisplay = "none";

  return (
      <Container maxWidth="sm" className={classes.container}>
        <Typography className={classes.title} variant="h5" align="center" onClick={toggleForm}>{title}</Typography>
        <Paper className={classes.paper} style={{display: formDisplay}} elevation={0} variant="outlined">
          <form onSubmit={submitFormHandler}>
            <Grid container direction="column" spacing={2}>
              <Grid item container spacing={2}>
                <Grid item xs>
                  <TextField
                      fullWidth
                      variant="outlined"
                      size="small"
                      id="author"
                      label="Имя"
                      name="author"
                      value={message.author}
                      onChange={onInputChange}
                  />
                </Grid>
                <Grid item>
                  <Button
                      style={{height: "100%"}}
                      size="medium"
                      variant="contained"
                      disableElevation
                      type="submit"
                  >
                    Отправить
                  </Button>
                </Grid>
              </Grid>
              <Grid item>
                <TextField
                    required
                    fullWidth
                    variant="outlined"
                    size="small"
                    multiline
                    rows={4}
                    id="message"
                    label="Комментарий"
                    name="message"
                    value={message.message}
                    onChange={onInputChange}
                />
              </Grid>
              <Grid item>
                <FileInput
                    name="image"
                    label="Выбрать картинку"
                    onChange={fileChangeHandler}
                    fileValue={message.image}
                />
              </Grid>
            </Grid>
          </form>
        </Paper>
      </Container>

  );
};

export default NewMessageForm;