import React from 'react';
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import FlashOnIcon from '@material-ui/icons/FlashOn';
import makeStyles from "@material-ui/core/styles/makeStyles";
import Toolbar from "@material-ui/core/Toolbar";
import Container from "@material-ui/core/Container";


const useStyles = makeStyles({
  header: {
    backgroundColor: "#ff6600",
    color: "#ffffff",
    padding: "15px 0",
    marginBottom: "30px"
  }
});

const AppToolbar = () => {
  const classes = useStyles();

  return (
      <>
        <AppBar position="static" className={classes.header}>
          <Toolbar>
            <Container maxWidth="xl">
              <Typography variant="h3" align="center">
                <FlashOnIcon fontSize="large"/>
                2ch - Добро пожаловать, снова...
              </Typography>
            </Container>
          </Toolbar>
        </AppBar>
      </>
  );
};

export default AppToolbar;