import React, {useEffect, useRef, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";


const useStyles = makeStyles({
  input: {
    display: "none"
  }
})

const FileInput = ({onChange, name, label, fileValue}) => {
  const classes = useStyles();
  const inputRef = useRef();

  const [filename, setFilename] = useState("");

  const activateInput = () => {
    inputRef.current.click();
  };

  const onFileChange = e => {
    if (e.target.files[0]) {
      setFilename(e.target.files[0].name);
    } else {
      setFilename("");
    }

    onChange(e);
  };

  useEffect(() => {
    if (fileValue === "") {
      setFilename("");
    }
  }, [fileValue]);

  return (
      <>
        <input
            type="file"
            name={name}
            className={classes.input}
            ref={inputRef}
            onChange={onFileChange}
        />
        <Grid container spacing={2}>
          <Grid item xs>
            <TextField
                variant="outlined"
                size="small"
                disabled
                fullWidth
                label={label}
                value={filename}
                onClick={activateInput}
            />
          </Grid>
          <Grid item>
            <Button
                style={{height: "100%"}}
                variant="contained"
                disableElevation
                onClick={activateInput}
            >
              Browse
            </Button>
          </Grid>
        </Grid>
      </>
  );
};

export default FileInput;