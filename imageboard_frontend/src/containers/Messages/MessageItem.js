import React from 'react';
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";

import {apiURL} from "../../config";


const useStyles = makeStyles({
  paper: {
    padding: "10px",
    backgroundColor: "#eaeaea"
  },
  info: {
    color: "#666666"
  }
});

const MessageItem = ({name, date, image, message}) => {
  const classes = useStyles();

  let imageBlock = null;
  if (image) {
    imageBlock = (
        <Grid item>
          <img
              style={{marginRight: "15px"}}
              src={`${apiURL}/uploads/${image}`}
              alt=""
              height="200px"
              width="auto"
          />
        </Grid>
    );
  }

  return (
      <Grid item>
        <Paper elevation={0} variant="outlined" className={classes.paper}>
          <Grid container direction="column" spacing={2}>
            <Grid item container className={classes.info}>
              <Grid item xs>{name}</Grid>
              <Grid item>{date}</Grid>
            </Grid>
            <Grid item container>
              {imageBlock}
              <Grid item xs>{message}</Grid>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
  );
};

export default MessageItem;