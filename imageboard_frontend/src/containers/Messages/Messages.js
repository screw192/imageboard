import React, {useEffect} from 'react';
import moment from "moment";
import {useDispatch, useSelector} from "react-redux";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";

import {fetchMessages} from "../../storage/actions/messagesActions";
import MessageItem from "./MessageItem";


const Messages = () => {
  const dispatch = useDispatch();
  const messages = useSelector(state => state.messages.messages);

  useEffect(() => {
    dispatch(fetchMessages());
  }, [dispatch]);

  const messagesBlock = messages.map(message => {
    const dateParsed = moment(message.datetime).format("DD/MM/YY ddd hh:mm:ss");

    return (
        <MessageItem
            key={message.id}
            name={message.author}
            date={dateParsed}
            image={message.image}
            message={message.message}
        />
    );
  });

  return (
      <Container maxWidth="md">
        <Grid container direction="column" spacing={2}>
          {messagesBlock}
        </Grid>
      </Container>
  );
};

export default Messages;