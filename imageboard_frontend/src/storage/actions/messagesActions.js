import axiosMessages from "../../axiosMessages";

export const FETCH_MESSAGES_REQUEST = "FETCH_MESSAGES_REQUEST";
export const FETCH_MESSAGES_SUCCESS = "FETCH_MESSAGES_SUCCESS";
export const FETCH_MESSAGES_FAILURE = "FETCH_MESSAGES_FAILURE";

export const POST_MESSAGE_REQUEST = "POST_MESSAGE_REQUEST";
export const POST_MESSAGE_SUCCESS = "POST_MESSAGE_SUCCESS";
export const POST_MESSAGE_FAILURE = "POST_MESSAGE_FAILURE";

export const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchMessagesSuccess = messagesData => ({type: FETCH_MESSAGES_SUCCESS, messagesData});
export const fetchMessagesFailure = () => ({type: FETCH_MESSAGES_FAILURE});

export const postMessageRequest = () => ({type: POST_MESSAGE_REQUEST});
export const postMessageSuccess = () => ({type: POST_MESSAGE_SUCCESS});
export const postMessageFailure = () => ({type: POST_MESSAGE_FAILURE});

export const fetchMessages = () => {
  return async dispatch => {
    try {
      dispatch(fetchMessagesRequest());

      const response = await axiosMessages.get("/");
      dispatch(fetchMessagesSuccess(response.data));
    } catch (e) {
      dispatch(fetchMessagesFailure());
    }
  };
};

export const postMessage = messageData => {
  return async dispatch => {
    try {
      dispatch(postMessageRequest());

      await axiosMessages.post("messages", messageData);
      dispatch(postMessageSuccess());
    } catch (e) {
      dispatch(postMessageFailure());
    }
  };
};