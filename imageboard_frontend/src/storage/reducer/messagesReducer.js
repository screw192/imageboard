import {FETCH_MESSAGES_FAILURE, FETCH_MESSAGES_REQUEST, FETCH_MESSAGES_SUCCESS} from "../actions/messagesActions";

const initialState = {
  messages: [],
  loading: false,
  error: false,
};

const messagesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MESSAGES_REQUEST:
      return {...state, loading: true};
    case FETCH_MESSAGES_SUCCESS:
      return {...state, loading: false, messages: action.messagesData};
    case FETCH_MESSAGES_FAILURE:
      return {...state, loading: false, error: true};
    default:
      return state;
  }
};

export default messagesReducer;